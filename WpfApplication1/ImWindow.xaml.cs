﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for ImWindow.xaml
    /// </summary>
    public partial class ImWindow : Window
    {
        private System.Printing.PrintQueue _queue;
        private System.Printing.PrintTicket _ticket;
        public ImWindow(PrintQueue queue, PrintTicket ticket)
        {
            InitializeComponent();
            _queue = queue;
            _ticket = ticket;
            
        }

        private void loaded(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
        }
        public void Print()
        {
            //this.Visibility = Visibility.Visible;
            PrintDialog printDialog = new PrintDialog();
            printDialog.PrintQueue = _queue;
            printDialog.PrintTicket = _ticket;
            lb_dt.Content = "Дата-время печати: " +DateTime.Now.ToShortDateString()+ " "+ DateTime.Now.ToShortTimeString();
            //printDialog.PrintVisual(im_test,"ttt");
            //now print the visual to printer to fit on the one page.
            printDialog.PrintVisual(gr_print, "Code ganked");
            //this.Visibility = Visibility.Collapsed;
        }
    }
}
