﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Brushes = System.Windows.Media.Brushes;

namespace WpfApplication1
{
    public static class PrintHelper
    {

        private static string _imageString;
        private static string _printerName;
        private static bool _pageOrientationIsLandskape = false;

        public static void Print(this string image, string printerName )
        {
            _printerName = printerName;
            _imageString = image;

            if (!string.IsNullOrWhiteSpace(_printerName))
            {
                PrintImage();
            }


        }

        private static void PrintImage()
        {
            PrintDocument pd = new PrintDocument();
            pd.OriginAtMargins = true;

            pd.OriginAtMargins = false;
            pd.PrinterSettings.PrinterName = _printerName;
            pd.PrintPage += pd_PrintPage;

            pd.DefaultPageSettings.Landscape = _pageOrientationIsLandskape;
            pd.Print();
        }

        
        
        private static void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            double cmToUnits = 100 / 2.54;
            byte[] imageBytes = Convert.FromBase64String(_imageString);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);


            float width = image.Width;
            float height = image.Height;


            float maxWidth = (float)(19 * cmToUnits);
            float maxHeight = (float)(26 * cmToUnits);

            if (e.PageSettings.Landscape)
            {
                maxWidth = (float)(28 * cmToUnits);
                maxHeight = (float)(17 * cmToUnits);
            }
            else
            {
                maxWidth = (float)(19 * cmToUnits);
                maxHeight = (float)(26 * cmToUnits);
            }

            if (width > maxWidth)
            {
                width = maxWidth;
                height = width / image.Width * height;
            }

            if (height > maxHeight)
            {
                var oldHeight = height;
                height = maxHeight;
                width = height / oldHeight * width;
            }

            float dx = (maxWidth - width) / 2;
            
            //e.Graphics.DrawImage(image, 30, 40, (float)(27 * cmToUnits), (float)(18 * cmToUnits));
            e.Graphics.DrawImage(image, 40 + dx, 80, width, height);

            ms.Close();
        }

    }
}
