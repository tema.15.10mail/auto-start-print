﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClearPrinterDataContract
{
    public class ClearPrinterStartParamsGenerator
    {
        public string PrinterName { get; private set; }
        public List<TimeSpan> EventsList { get; private set; } 
        public string[] StartParams { get; private set; }

        public ClearPrinterStartParamsGenerator(string printerName,List<TimeSpan> events)
        {
            PrinterName = printerName;
            EventsList = events;
            StartParams = new string[events.Count+1];
            StartParams[0] = printerName;
            for (int i = 0; i < events.Count; i++)
            {
                StartParams[i + 1] = events[i].ToString();
            }
        }

        public ClearPrinterStartParamsGenerator(string[] startParams)
        {
            StartParams = startParams;
            PrinterName = startParams[0];
            
            EventsList = new List<TimeSpan>();
            for (int i = 1; i < startParams.Count(); i++)
            {
                EventsList.Add(TimeSpan.Parse(startParams[i]));
            }
            StartParams = startParams;
        }


    }
}
