﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PrintHelpApp
{
    public static class ConvertToBitmap
    {
       

            //public static BitmapImage GetBitmap(this Blob blob)
            //{
            //    prepareTempdirectory();
            //    if (blob.DocumentExtention.ToUpperInvariant().Equals("PDF"))
            //    {
            //        return new BitmapImage(new Uri("pack://application:,,,/PalliativeCareClient;component/Images/PdfDefault.png"));
            //    }
            //    var tfp = Path.Combine(GetTempDirectoryPath(), string.Format("{0}.{1}", Guid.NewGuid().ToString(), blob.DocumentExtention));

            //    using (FileStream fs = File.Create(tfp))
            //    {
            //        var result = System.Convert.FromBase64String(blob.Document);
            //        fs.Write(result, 0, result.Length);
            //    }
            //    BitmapImage bmi = new BitmapImage();
            //    bmi.BeginInit();
            //    bmi.UriSource = new Uri(tfp, UriKind.Absolute);
            //    bmi.EndInit();

            //    return bmi;

            //}

            //private const string TempDirectoryName = "PalliativeImageCache";

            //private static string GetTempDirectoryPath()
            //{
            //    string tempPath = System.IO.Path.GetTempPath();
            //    return Path.Combine(tempPath, TempDirectoryName);
            //}

            //private static void prepareTempdirectory()
            //{
            //    string tempPath = System.IO.Path.GetTempPath();
            //    var tdp = Path.Combine(tempPath, TempDirectoryName);
            //    DirectoryInfo di;
            //    if (!Directory.Exists(tdp))
            //    {
            //        di = Directory.CreateDirectory(tdp);
            //    }
            //    else
            //    {
            //        di = new DirectoryInfo(tdp);
            //    }

            //}



            public static BitmapImage GetBitmap(this string blob, bool lowQuality = false)
            {
               
                var imageByteArray = AnotherDecode64(blob);
                return ConvertByteToBitmap(imageByteArray, lowQuality);
            }

            private static BitmapImage ConvertByteToBitmap(byte[] image, bool lowquality = false)
            {
                MemoryStream ms = new MemoryStream(image);
                BitmapImage bmi = new BitmapImage();


                bmi.BeginInit();
                bmi.StreamSource = ms;
                if (lowquality)
                {
                    bmi.DecodePixelWidth = 300;
                }
                bmi.EndInit();

                return bmi;
            }

            private static byte[] AnotherDecode64(string base64Decoded)
            {
                string temp = base64Decoded.TrimEnd('=');
                int asciiChars = temp.Length - temp.Count(c => Char.IsWhiteSpace(c));
                switch (asciiChars % 4)
                {
                    case 1:
                        //This would always produce an exception!!
                        //Regardless what (or what not) you attach to your string!
                        //Better would be some kind of throw new Exception()
                        return new byte[0];
                    case 0:
                        asciiChars = 0;
                        break;
                    case 2:
                        asciiChars = 2;
                        break;
                    case 3:
                        asciiChars = 1;
                        break;
                }
                temp += new String('=', asciiChars);

                return System.Convert.FromBase64String(temp);
            }
        }
    }

