﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System;
using ClearPrinterDataContract;


namespace CleanPrinterServiceController
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public enum SimpleServiceCustomCommands
        { StopWorker = 128, RestartWorker, CheckWorker };

        private string _printerName;
        private ServiceController _sc;

        public MainWindow()
        {
            InitializeComponent();
            Application app = Application.Current;
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;
            app.MainWindow = this;
            ServiceController[] scServices;
            scServices = ServiceController.GetServices();
            var printService = scServices.FirstOrDefault(t => t.ServiceName == ClearPrinterConstants.ServiceName);
            if (printService!=null)
            {
                _sc = new ServiceController(ClearPrinterConstants.ServiceName);
                
            }

           
        }

        
        

        

        //private void GetImage(string fileName)
        //{
        //    using (FileStream fs = File.OpenRead(fileName))
        //    {
        //        using (var ms = new MemoryStream())
        //        {
        //            fs.CopyTo(ms);
        //            _image = Convert.ToBase64String(ms.ToArray());
        //        }
        //    }
        //}
        private void btn_setPrinter_Clicked(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == true)
            {
                lb_printer.Content = printDialog.PrintQueue.FullName;
                _printerName = printDialog.PrintQueue.FullName;
            }
        }

        private void PrintImideatly(object sender, RoutedEventArgs e)
        {
            _sc.ExecuteCommand((int)ClearPrintServiceCustomCommands.Printimideatly);
            //Console.WriteLine("Status = " + sc.Status);
            //Console.WriteLine("Can Pause and Continue = " + sc.CanPauseAndContinue);
            //Console.WriteLine("Can ShutDown = " + sc.CanShutdown);
            //Console.WriteLine("Can Stop = " + sc.CanStop);
            //if (sc.Status == ServiceControllerStatus.Stopped)
            //{
            //    sc.Start();
            //    while (sc.Status == ServiceControllerStatus.Stopped)
            //    {
            //        Thread.Sleep(1000);
            //        sc.Refresh();
            //    }
            //}
            //// Issue custom commands to the service 
            //// enum SimpleServiceCustomCommands  
            ////    { StopWorker = 128, RestartWorker, CheckWorker };
            //sc.ExecuteCommand((int)SimpleServiceCustomCommands.StopWorker);
            //sc.ExecuteCommand((int)SimpleServiceCustomCommands.RestartWorker);
            //sc.Pause();
            //while (sc.Status != ServiceControllerStatus.Paused)
            //{
            //    Thread.Sleep(1000);
            //    sc.Refresh();
            //}
            //Console.WriteLine("Status = " + sc.Status);
            //sc.Continue();
            //while (sc.Status == ServiceControllerStatus.Paused)
            //{
            //    Thread.Sleep(1000);
            //    sc.Refresh();
            //}
            //Console.WriteLine("Status = " + sc.Status);
            //sc.Stop();
            //while (sc.Status != ServiceControllerStatus.Stopped)
            //{
            //    Thread.Sleep(1000);
            //    sc.Refresh();
            //}
            //Console.WriteLine("Status = " + sc.Status);
            //String[] argArray = new string[] { "ServiceController arg1", "ServiceController arg2" };
            //sc.Start(argArray);
            //while (sc.Status == ServiceControllerStatus.Stopped)
            //{
            //    Thread.Sleep(1000);
            //    sc.Refresh();
            //}
            //Console.WriteLine("Status = " + sc.Status);
            //// Display the event log entries for the custom commands 
            //// and the start arguments.
            //EventLog el = new EventLog("Application");
            //EventLogEntryCollection elec = el.Entries;
            //foreach (EventLogEntry ele in elec)
            //{
            //    if (ele.Source.IndexOf("SimpleService.OnCustomCommand") >= 0 |
            //        ele.Source.IndexOf("SimpleService.Arguments") >= 0)
            //        Console.WriteLine(ele.Message);
            //}
        }

        private void ClickButton_on(object sender, RoutedEventArgs e)
        {

            _sc.Start(new ClearPrinterStartParamsGenerator(_printerName, new List<TimeSpan>() { DateTime.Now.TimeOfDay }).StartParams);

        }
        private void DisableBtns()
        {
            cb_5.IsEnabled = false;
            cb_7.IsEnabled = false;
            cb_9.IsEnabled = false;
            cb_12.IsEnabled = false;
            cb_15.IsEnabled = false;
            cb_18.IsEnabled = false;
            cb_20.IsEnabled = false;
            cb_22.IsEnabled = false;
            btn_setPrinter.IsEnabled = false;
        }
        private void EnableBtns()
        {
            cb_5.IsEnabled = true;
            cb_7.IsEnabled = true;
            cb_9.IsEnabled = true;
            cb_12.IsEnabled = true;
            cb_15.IsEnabled = true;
            cb_18.IsEnabled = true;
            cb_20.IsEnabled = true;
            cb_22.IsEnabled = true;
            btn_setPrinter.IsEnabled = true;
        }
        private bool HasPrintTime()
        {
            bool result = cb_7.IsChecked != null && (bool)cb_7.IsChecked;
            if (cb_5.IsChecked != null && (bool)cb_5.IsChecked)
            {
                result = true;
            }
            if (cb_9.IsChecked != null && (bool)cb_9.IsChecked)
            {
                result = true;
            }
            if (cb_12.IsChecked != null && (bool)cb_12.IsChecked)
            {
                result = true;
            }
            if (cb_15.IsChecked != null && (bool)cb_15.IsChecked)
            {
                result = true;
            }
            if (cb_18.IsChecked != null && (bool)cb_18.IsChecked)
            {
                result = true;
            }
            if (cb_20.IsChecked != null && (bool)cb_20.IsChecked)
            {
                result = true;
            }
            if (cb_22.IsChecked != null && (bool)cb_22.IsChecked)
            {
                result = true;
            }
            return result;

        }
        private void InitWorker()
        {
            

        }

        private void worker_dispused(object sender, EventArgs e)
        {
            
        }

        


        private void state_Changed(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
            {
                this.Visibility = Visibility.Hidden;
                this.WindowState = WindowState.Normal;
            }
        }

    }
}
